import { useState, useCallback } from "react";

export const useGenericUpdate = () => {
  const [{ updates }, setState] = useState({ updates: 0 });

  return useCallback(() => setState({ updates: updates + 1 }), [updates]);
};
