import { useCallback } from "react";

import {
  RTCCafeSignallingClient,
  RTC_CAFE_SIGNALLING_CLIENT_EVENTS,
} from "rtc-cafe-client";

import { useSubscription } from "./useSubscription";

const signallers = {};

export const useRTCCafeSignaller = ({
  apiVersion,
  host,
  signalModifier,
  ioOptions,
}) => {
  const updateSubscribers = useSubscription(["signaller", host]);
  const signaller = signallers[host];

  const connect = useCallback(() => {
    if (signallers[host]) {
      return;
    }
    signallers[host] = new RTCCafeSignallingClient({
      apiVersion,
      host,
      signalModifier,
      ioOptions,
    });
    for (const eventName of RTC_CAFE_SIGNALLING_CLIENT_EVENTS) {
      signallers[host].on(eventName, updateSubscribers);
    }
    updateSubscribers();
  }, [apiVersion, host, signalModifier, ioOptions]);

  const disconnect = useCallback(() => {
    if (!signallers[host]) {
      return;
    }
    signallers[host].off();
    signallers[host].disconnect();
    delete signallers[host];
    updateSubscribers();
  }, [host]);

  return {
    signaller,
    connect,
    disconnect,
    state: signaller ? signaller.state : "pre-setup",
  };
};
