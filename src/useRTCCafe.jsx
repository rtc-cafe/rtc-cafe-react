import { useCallback } from "react";

import { RTCCafeClient, RTC_CAFE_CLIENT_EVENTS } from "rtc-cafe-client";

import { useSubscription } from "./useSubscription";
import { useGenericUpdate } from "./useGenericUpdate";

const DEFAULT_RTC_CONFIGURATION = {
  iceServers: [
    {
      urls: ["stun:stun.l.google.com:19302"],
    },
  ],
};

const clients = {};

export const useRTCCafe = (options) => {
  options = { rtcConfig: DEFAULT_RTC_CONFIGURATION, ...options };
  const { room, rtcConfig } = options;
  const client = clients[room];
  delete options.rtcConfig;
  delete options.room;

  const updateSubscribers = useSubscription(["client", room]);
  const genericUpdate = useGenericUpdate();

  const joinRoom = useCallback(() => {
    if (clients[room]) {
      return;
    }
    clients[room] = new RTCCafeClient(rtcConfig, options);
    clients[room].joinRoom(room);
    for (const eventName of RTC_CAFE_CLIENT_EVENTS) {
      clients[room].on(eventName, () => {
        updateSubscribers();
        genericUpdate();
      });
    }
    updateSubscribers();
  }, [options]);

  const disconnect = useCallback(() => {
    if (!clients[room]) {
      return;
    }
    clients[room].off();
    clients[room].disconnectFromPeers();
    if (!options.signaller) {
      // Disconnect signaller unless it was passed into the hook through `options`
      clients[room].disconnectSignaller();
    }
    delete clients[room];
    updateSubscribers();
  }, [options]);

  return {
    client,
    joinRoom,
    disconnect,
    state: client ? client.state : "pre-setup",
  };
};
