import { useState, useEffect, useCallback } from "react";
import { v4 as uuidv4 } from "uuid";

import { useGenericUpdate } from "./useGenericUpdate";

const subscribers = {};

export const useSubscription = (key) => {
  key = key.toString();
  const genericUpdate = useGenericUpdate();
  const [id] = useState(uuidv4());

  const buildCleanupFunction = (oldKey) => () => {
    const subscribersForKey = (subscribers[oldKey] = subscribers[oldKey] || []);
    const index = subscribersForKey.findIndex(
      ({ id: subscriberId }) => subscriberId === id
    );
    if (index === -1) {
      return;
    }
    subscribersForKey.splice(index, 1);
  };

  useEffect(() => {
    if (!key) {
      return () => {};
    }
    const subscribersForKey = (subscribers[key] = subscribers[key] || []);
    subscribersForKey.push({ id, callback: genericUpdate });
    return buildCleanupFunction(key);
  }, [key]);

  const update = useCallback(() => {
    const subscribersForKey = (subscribers[key] = subscribers[key] || []);
    for (const { callback } of subscribersForKey) {
      callback();
    }
  }, [key]);

  return update;
};
