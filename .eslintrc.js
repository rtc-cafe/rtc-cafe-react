module.exports = {
  env: {
    browser: true,
  },
  extends: [
    'airbnb-base',
    'plugin:prettier/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
      semi: 'error',
      'no-param-reassign': 0,
      'no-multi-assign': 0,
      'no-restricted-syntax': 0,
      'no-continue': 0,
      'no-underscore-dangle': 0,
      'import/no-unresolved': 0,
      'import/extensions': 0,
      'import/prefer-default-export': 0,
      'max-len': ["error", {"code": 100}],
  },
  globals: {
    __karma__: true,
  },
};
