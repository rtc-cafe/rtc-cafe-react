const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const path = require("path");
const TerserPlugin = require("terser-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");
const webpack = require("webpack");

// Export a function in order to change the behavior according the `mode` variable
module.exports = (env, argv) => {
  const mode = argv ? argv.mode : "production";

  const distPath = path.join(__dirname, "dist");
  const srcPath = path.join(__dirname, "src");

  const config = {
    entry: ["./src/index.js"],
    mode,
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          include: [srcPath],
        },
      ],
    },
    output: {
      filename: "bundle.js",
      path: distPath,
      library: "rtcCafeReact",
      libraryTarget: "umd",
    },
    optimization: {
      minimizer: [
        new TerserPlugin({
          parallel: true,
          terserOptions: {
            ecma: 2018,
          },
        }),
      ],
    },
    plugins: [
      new CleanWebpackPlugin(),
      new ESLintPlugin({
        files: 'src',
      }),
    ],
    devtool: "inline-source-map",
    resolve: {
      extensions: ["*", ".js", ".jsx"],
      modules: ["node_modules"],
    },
    externals: {
      react: "react",
      "react-dom": "react-dom",
      "rtc-cafe-client": "rtc-cafe-client",
    },
  };

  if (mode === "development") {
    Object.assign(config, {
      // https://github.com/webpack-contrib/karma-webpack#source-maps
      // https://github.com/airbnb/enzyme/blob/master/docs/guides/karma.md
      devtool: "inline-source-map",
    });

    config.optimization.minimize = false;
  }
  return config;
};
